import {inject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-http-client';
import * as toastr from 'toastr';

@inject(HttpClient)
export class App {

  constructor(httpClient){
    this.httpClient = httpClient;
    this.heading = "URL Shortener";
    this.originalUrl = '';
    this.originalUrlResponse = '';
    this.shortUrl = '';
    this.shortUrlResponse = '';
    this.error = '';
    this.baseurl = "http://3.131.113.43:8081";
    this.urls = [];
    this.init();
  }

  init() {
    this.httpClient.get( this.baseurl + "/urls")
          .then(responseMessage => {        
            console.log(JSON.parse(responseMessage.response));
            this.urls.push.apply(this.urls,JSON.parse(responseMessage.response));
          }).catch( error => toastr.error("Could not load initial data"));
  }

  shortenUrl() {
    this.reset();
    if (this.originalUrl) {
    let url = {"originalUrl": this.originalUrl}
    this.httpClient.post( this.baseurl + "/short-url", url)
      .then(responseMessage => {        
          let response = JSON.parse(responseMessage.response);
          
          this.originalUrlResponse = response.shortUrl;


          if(!this.urls.some(e => e.shortUrl === this.originalUrlResponse)) {
            toastr.success("Generated Short url");
            this.urls.push(response);
          } else {
            toastr.info("Url already shortened");
            
          }
          this.originalUrl = '';
      }).catch( error => this.handleError(error, this.originalUrl));
    }
  }

  revealUrl() {
    this.reset();
    if (this.shortUrl) {
    let url = {"shortUrl": this.shortUrl}
    this.httpClient.post(this.baseurl + "/original-url", url)
      .then(responseMessage => {        
          toastr.success("Revealed original url");
          this.shortUrlResponse= JSON.parse(responseMessage.response).originalUrl;
          this.shortUrl = '';
      }).catch( error => this.handleError(error, this.shortUrl));
    }
  }

  delete(url) {
    this.httpClient.delete(url.shortUrl)
      .then(responseMessage => {        
          toastr.success("Url delete");
          this.urls = this.urls.filter(e => {
            return e !== url
        })
      }).catch( error => this.handleError(error, this.shortUrl));
  
  }

  handleError(error, url) {
    this.error = JSON.parse(error.response).errorMessage + " Please make sure to provide a valid url including the protocol.";
    toastr.error(this.error);
    url = '';
  }

  reset() {
    this.shortUrlResponse= '';
    this.originalUrlResponse= '';
  }
}
